import http

from rest_framework.test import APITestCase

from core.util import ViewSetTestMixin


class ExampleTestCase(APITestCase, ViewSetTestMixin):
    _basename = 'example'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        pass

    def test_get_action_200(self):
        url = self.reverse_view_url('get_action_example')
        response = self.client.get(url)
        self.assertEqual(response.status_code, http.HTTPStatus.OK, response.data)

    def test_post_action_200(self):
        url = self.reverse_view_url('post_action_example')
        data = dict()
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, http.HTTPStatus.OK, response.data)
